const isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()) {
      return next();  
    } else {
        return res.send(403);
    }
};

const isTeacher = (req, res, next) => {
    console.log(req.passport);
    if(req.user.role === 'teacher') {
        return next();
    } else {
        return res.send(403);
    }
}

module.exports = {
    isAuthenticated,
    isTeacher,
}