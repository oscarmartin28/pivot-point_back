const path = require('path');
const multer = require('multer');
require('dotenv').config();
const fs = require('fs');
const cloudinary = require('cloudinary').v2;

const VALID_FILE_TYPES = ['image/png', 'image/jpg', 'image/jpeg'];

const upload = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      cb(null, Date.now() + file.originalname);
    },
    destination: (req, file, cb) => {
      cb(null, path.join(__dirname, '../public/uploads'));
    },
  }),
  fileFilter: (req, file, cb) => {
    if (!VALID_FILE_TYPES.includes(file.mimetype)) {
      cb(new Error('Invalid file type'));
    } else {
      cb(null, true);
    }
  },
});


const uploadToCloudinary = async (req, res, next) => {
	if (req.file) {
		const filePath = req.file.path;
    const image = await cloudinary.uploader.upload(filePath);

    await fs.unlinkSync(filePath);
	
    req.file_url = image.secure_url;
		return next();
  } else {
    return next();
  }
};

module.exports = { upload: upload, uploadToCloudinary };