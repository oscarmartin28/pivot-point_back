const Student = require('../models/Student.js');

const studentGet = async (req, res) => {
    try {
        const pets = await Pet.find();
        return res.status(200).json(pets);
    } catch (err) {
        return res.status(500).json(err);
    }
};

const idGet = async (req, res) => {
    const id = req.params.id;
  
    try {
      const studentFinded = await Student.findById(id);
      return res.status(200).json(studentFinded);
  
    } catch (error) {
      return res.status(500).json(error);
    }
};

const createPost = async (req, res, next) => {
    console.log(req.file);

    // ESTO CON CLAUDINARY  =>  const mascotaPicture = req.file_url;
    
    const studentPicture = req.file ? req.file.filename : null;
  
      try {
        // Crearemos una instancia de mascota con los datos enviados
        const newStudent = new Pet ({
          name: req.body.name,
          class: req.body.class,
          course: req.body.subject,
          picture: petPicture,
        });
    
        // Guardamos la mascota en la DB
        await newStudent.save();
        return res.redirect('/students');
      } catch (err) {
            // Lanzamos la función next con el error para que gestione todo Express
        next(err);
      }
};

const editPut = async (req, res, next) => {
    try {
      const id = req.body.id;
  
      const updatedStudent = await Student.findByIdAndUpdate(
        id, // La id para encontrar el documento a actualizar
        { 
            name: req.body.name,
            class: req.body.class,
            subject: req.body.subject,
            picture: petPicture,
        },
         // Campos que actualizaremos
        { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
      );
  
      return res.status(200).json(updatedStudent);
    } catch (err) {
      next(err);
    }
};

const studentDelete = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      await Student.findByIdAndDelete(id);
  
      res.status(200).json("Student deleted");
    } catch (error) {
      next(error);
    }
};

module.exports = {
    studentGet,
    idGet,
    createPost,
    editPut,
    studentDelete,
}