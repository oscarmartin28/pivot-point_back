const express = require('express');
const studentsController = require('../controllers/students.controller');
const fileMiddlewares = require('../middlewares/file.middleware');
const router = express.Router();

router.get('/', studentsController.studentGet);
router.get('/', studentsController.studentDelete);
router.get('/', studentsController.idGet);
router.get('/', studentsController.createPost);
router.get('/', studentsController.editPut);

module.exports = router;