const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    picture: { type: String },
    students: [{ type: mongoose.Types.ObjectId, ref: 'Students' }],
    role: {
      enum: ["user", "teacher"],
      type: String,
      default: "teacher",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("Users", userSchema);

module.exports = User;
