const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const studentSchema = new Schema(
    {
        name: { type: String, required: true },
        class: { type: String, required: true },
        subject: [
            {
                title: { type: String, required: true },
                total: { type: Number, required: true },
            }
        ],
        picture: { type: String },
    },
    {
        timestamps: true,
    }
);

const Student = mongoose.model('Students', studentSchema);

module.exports = Student;