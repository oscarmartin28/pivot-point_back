const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const User = require("./models/User");

const saltRound = 10;

const validate = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

passport.serializeUser((user, done) => {
  return done(null, user._id);
});

passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});

const DEFAULT_IMAGE = 'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png';

passport.use(
  "register",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      pictureField: "picture",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        if (email.length < 10) {
          const error = new Error("Email must be 10 characters min");
          return done(error);
        }

        const validEmail = validate(email);

        if (!validEmail) {
          const error = new Error("Invalid Email");
          return done(error);
        }

        const previousUser = await User.findOne({
          email: email.toLowerCase(),
        });

        if (previousUser) {
          const error = new Error("The user already exists");
          return done(error);
        }

        const userPicture = req.file ? req.file_url : DEFAULT_IMAGE;
        const hash = await bcrypt.hash(password, saltRound);
        const newUser = new User({
          email: email.toLowerCase(),
          username: req.body.username,
          password: hash,
          picture: userPicture,
        });

        const savedUser = await newUser.save();

        return done(null, savedUser);
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(
  "login",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        const validEmail = validate(email); 

        if (email.length < 10) {
          const error = new Error("El email debe contener 10 caracteres como mínimo.");
          return done(error);
        }

        if (password.length < 6) {
          const error = new Error("La contraseña tiene que tener 6 caracteres mínimo.");
          return done(error);
        }

        if (!validEmail) {
          const error = new Error("Email invalido.");
          return done(error);
        }

        const currentUser = await User.findOne({ email: email.toLowerCase() });

        if (!currentUser) {
          const error = new Error("El usuario no existe!");
          return done(error);
        }

        const isValidPassword = await bcrypt.compare(
          password,
          currentUser.password
        );

        if (!isValidPassword) {
          const error = new Error("The email or password is invalid!");
          return done(error);
        }

        return done(null, currentUser);
      } catch (err) {
        return done(err);
      }
    }
  )
);