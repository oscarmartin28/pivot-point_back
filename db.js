const mongoose = require('mongoose');

const DB_URL = 'mongodb://localhost:27017/pivot_point';;
console.log(DB_URL)
/* 'mongodb://localhost:27017/lucky' ||  */

const connect = () => {
  mongoose
    .connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
  .then(() => {
  console.log('Connected to DB');
  })
  .catch((error) => {
  console.log('Error connecting to DB', error);
  })
}

module.exports = {DB_URL, connect};